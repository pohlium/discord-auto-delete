#!/usr/bin/env python3

from __future__ import annotations
import datetime
import time
import logging
import json
import enum
from typing import Any, Dict, List, Union, Optional

import requests
import toml
import toml.decoder


LOGLEVEL = logging.INFO


class MessageDeleteError(Exception):
    def __init__(self, message: Message, resp: requests.Response):
        super()
        self.message = message
        self.response = resp


class Config:
    def __init__(self, data: dict):
        self.skip_channels: List[str] = [str(x) for x in data['skip_channels']]
        self.skip_guilds: List[str] = [str(x) for x in data['skip_guilds']]
        self.before: datetime.datetime = (datetime.datetime.fromisoformat(data['before'])
                                          if data['before'] else datetime.datetime.now())
        self.keep_pinned: bool = data['keep_pinned']
        self.unarchive_threads: bool = data['unarchive_threads']
        self.from_json: bool = data['from_json']
        self.purge_guilds: bool = data['purge_guilds']
        self.purge_dms: bool = data['purge_dms']
        self.user_id: str = str(data['user_id'])
        self.token: str = data['token']
        if not self.user_id or not self.token:
            raise RuntimeError('You have to fill out your user_id and token in the config.toml!')

    @classmethod
    def from_toml(cls, path: str) -> Config:
        try:
            with open(path, 'r') as filp:
                data = toml.load(filp)
        except FileNotFoundError as exc:
            raise RuntimeError(f'Please copy the example config to {path} and fill it with '
                                'your data.') from exc
        except toml.decoder.TomlDecodeError as exc:
            raise RuntimeError('Invalid toml detected! Please use the example_config.toml as a '
                               'template!') from exc
        return cls(data)

    def format(self) -> str:
        attr_string = '\n'.join(
            [f'{attribute}: {value}' for attribute, value in self.__dict__.items()
             if attribute != 'token']
        )
        return f'Config:\n{attr_string}\ntoken: REDACTED'

    def __str__(self) -> str:
        attr_string = ', '.join(
            [f'{attribute}: {value}' for attribute, value in self.__dict__.items()
             if attribute != 'token']
        )
        return f'<Config {attr_string}, token: REDACTED>'

    def __repr__(self) -> str:
        return str(self)


class ChannelAvailability(enum.Enum):
    AVAILABLE = 0
    INDEXING = 1
    UNAVAILABLE = 2


class RequestType(enum.Enum):
    GET = 0
    POST = 1
    DELETE = 2
    PATCH = 3


class ChannelType(enum.Enum):
    GUILD_TEXT = 0
    DM = 1
    GUILD_VOICE = 2
    GROUP_DM = 3
    GUILD_CATEGORY = 4
    GUILD_ANNOUNCEMENT = 5
    ANNOUNCEMENT_THREAD = 10
    PUBLIC_THREAD = 11
    PRIVATE_THREAD = 12
    GUILD_STAGE_VOICE = 13
    GUILD_DIRECTORY = 14
    GUILD_FORUM = 15


class AuthUser:
    def __init__(self, token: str, id_: str):
        self.token = token
        self.id = id_


class PartialChannel:
    def __init__(self, id_: str, api: Discord):
        self._api = api
        self.id = id_
        self.logger = logging.getLogger(f'PartialChannel {self.id}')
        self.logger.setLevel(LOGLEVEL)

    def __eq__(self, other: Union[Channel, PartialChannel]) -> bool:
        return self.id == other.id

    def __repr__(self):
        return f'PartialChannel {self.id}'

    def check_available(self) -> ChannelAvailability:
        self.logger.debug('Checking availability of channel %s', self)
        params = {'author_id': str(self._api.user.id)}
        endpoint = f'/channels/{self.id}/messages/search'
        resp = self._api.safe_get(endpoint, params=params, skip_indexing=True)
        if resp.status_code == 202:
            return ChannelAvailability.INDEXING
        if not resp.ok:
            return ChannelAvailability.UNAVAILABLE
        return ChannelAvailability.AVAILABLE

    def get_messages(self, offset: int, max_id: Optional[str]=None) -> List[Message]:
        self.logger.debug('Requesting messages for channel %s at offset %i', self, offset)
        params = {'offset': str(offset),
                  'max_id': max_id,
                  'author_id': str(self._api.user.id),
                  'sort_by': 'timestamp',
                  'sort_order': 'desc'}
        endpoint = f'/channels/{self.id}/messages/search'
        self.logger.debug('Requesting messages at endpoint `%s`', endpoint)
        resp = self._api.safe_get(endpoint, params=params)
        if not resp.ok:
            if resp.status_code == 403:
                self.logger.warning('Missing access for channel with ID %s, can not get any messages.',
                                    self.id)
            else:
                self._api.log_request_error('Error getting channel messages.', resp)
            return []
        message_dicts = resp.json()['messages']
        messages = [Message(x[0], self._api) for x in message_dicts]
        return messages

    def get_all_messages(self, max_id: Optional[str]=None) -> List[Message]:
        self.logger.info('Collecting messages for %s', self)
        messages = []
        offset = 0
        message_page = self.get_messages(offset, max_id)
        while len(message_page) > 0:
            messages += message_page
            offset += len(message_page)
            message_page = self.get_messages(offset, max_id)
            self.logger.info('Found %i messages in %s', len(messages), self)
        return messages

    def unarchive(self) -> bool:
        endpoint = f'/channels/{self.id}'
        data = json.dumps({'archived': False})
        resp = self._api.safe_patch(endpoint, data=data)
        return resp.ok


# discord API models
class User:
    def __init__(self, data: Dict[str, str], api: Discord):
        self._api = api
        self.id = data['id']
        self.name = data['username']
        self.discriminator = data['discriminator']


class Message:
    def __init__(self, data: Dict[str, Any], api: Discord):
        self._api = api
        self.id = data['id']
        self.channel_id = data['channel_id']
        self.guild_id = data.get('guild_id', None)
        self.author = User(data['author'], self._api)
        self.content = data['content']
        self.timestamp = data['timestamp']
        self.pinned = data['pinned']
        self.logger = logging.getLogger(f'Message {self.id}')
        self.logger.setLevel(LOGLEVEL)

    def __repr__(self) -> str:
        return (f'Message {self.id} by {self.author.name}#{self.author.discriminator} '
                f'in channel {self.channel_id} @ {self.timestamp}')

    def delete(self) -> None:
        self.logger.debug('Deleting message %s', self)
        endpoint = f'/channels/{self.channel_id}/messages/{self.id}'
        resp = self._api.safe_delete(endpoint)
        if not resp.ok:
            raise MessageDeleteError(self, resp)
        self.logger.debug('Successfully deleted message from %s: %s',
                          self.timestamp,
                          self.content)


class Channel(PartialChannel):
    def __init__(self, data: Dict[str, Any], api: Discord):
        super().__init__(data['id'], api)
        self.guild_id: Optional[str] = data.get('guild_id', None)
        self.name: Optional[str] = data.get('name', None)
        self.type: ChannelType = ChannelType(data.get('type', 0))
        self.recipients: List[User] = [User(e, api) for e in data.get('recipients', [])]
        self.logger = logging.getLogger(f'PartialChannel {self.id}')

    def __repr__(self) -> str:
        if self.type in [ChannelType.DM, ChannelType.GROUP_DM]:
            channel_type = 'DMChannel'
            name = 'with ' + ', '.join([user.name for user in self.recipients])
            guild = ''
        else:
            channel_type = 'Channel'
            name = self.name
            guild = f'in {self.guild_id}'
        return f'{channel_type} {name} ID {self.id} {guild}'


class Guild:
    def __init__(self, data: Dict[str, str], api: Discord):
        self._api = api
        self.id = data['id']
        self.name = data['name']
        self.logger = logging.getLogger(f'Guild {self.id}')
        self.logger.setLevel(LOGLEVEL)

    def __repr__(self) -> str:
        return f'Guild {self.name} - {self.id}'

    def get_messages(self, offset: int, max_id: Optional[str]=None) -> List[Message]:
        self.logger.debug('Requesting messages for guild %s at offset %i', self, offset)
        params = {'offset': str(offset),
                  'max_id': max_id,
                  'author_id': str(self._api.user.id),
                  'sort_by': 'timestamp',
                  'sort_order': 'desc'}
        endpoint = f'/guilds/{self.id}/messages/search'
        self.logger.debug('Requesting messages at endpoint `%s`', endpoint)
        resp = self._api.safe_get(endpoint, params=params)
        if not resp.ok:
            self._api.log_request_error('Error getting guild messages.', resp)
            return []
        message_dicts = resp.json()['messages']
        messages = [Message(x[0], self._api) for x in message_dicts]
        return messages

    def get_all_messages(self, max_id: Optional[str]=None) -> List[Message]:
        self.logger.info('Collecting messages for %s', self)
        messages = []
        offset = 0
        message_page = self.get_messages(offset, max_id)
        while len(message_page) > 0:
            messages += message_page
            offset += len(message_page)
            message_page = self.get_messages(offset, max_id)
            self.logger.info('Found %i messages in %s', len(messages), self)
        return messages


class Discord:
    def __init__(self, user: AuthUser, timeout: float=4.05):
        self.user = user
        self.base_url = 'https://discord.com/api/v9'
        self.session = requests.session()
        self.session.headers.update({'Authorization': user.token})
        self.earliest_next_request = time.time()
        self.timeout = timeout
        self.logger = logging.getLogger(f'discord-{user.id}')
        self.logger.setLevel(LOGLEVEL)

    def wait_for_next_request(self):
        time_until_next_allowed_request = self.earliest_next_request - time.time()
        if time_until_next_allowed_request > 0:
            self.logger.debug('Sleeping for %f', time_until_next_allowed_request)
            time.sleep(time_until_next_allowed_request)

    def safe_get(self,
                 endpoint: str,
                 *,
                 skip_indexing: bool=False,
                 **kwargs) -> requests.Response:
        return self._safe_request(endpoint, RequestType.GET, skip_indexing=skip_indexing, **kwargs)

    def safe_delete(self, endpoint: str) -> requests.Response:
        return self._safe_request(endpoint, RequestType.DELETE)

    def safe_patch(self, endpoint: str, **kwargs) -> requests.Response:
        return self._safe_request(endpoint, RequestType.PATCH, **kwargs)

    def log_request_error(self, msg: str, resp: requests.Response):
        self.logger.error('%s Code: %i, Reason: %s, Content: %s',
                          msg,
                          resp.status_code,
                          resp.reason,
                          resp.content)

    def get_dm_channels(self) -> List[Channel]:
        self.logger.debug('Requesting DM channels for user %s', self.user.id)
        endpoint = '/users/@me/channels'
        resp = self.safe_get(endpoint)
        if not resp.ok:
            self.log_request_error('Error getting DM channels.', resp)
            return []
        dm_channels = [Channel(x, self) for x in resp.json()]
        return dm_channels

    def get_guilds(self) -> List[Guild]:
        self.logger.debug('Requesting guilds for user %s', self.user.id)
        endpoint = '/users/@me/guilds'
        resp = self.safe_get(endpoint)
        if not resp.ok:
            self.log_request_error('Error getting guilds.', resp)
            return []
        guilds = [Guild(x, self) for x in resp.json()]
        return guilds

    def _safe_request(self,
                      endpoint: str,
                      request_type: RequestType,
                      skip_indexing: bool=False,
                      **kwargs) -> requests.Response:
        request_methods = {
            RequestType.GET: self.session.get,
            RequestType.POST: self.session.post,
            RequestType.DELETE: self.session.delete,
            RequestType.PATCH: self.session.patch
        }
        url = self.base_url + endpoint
        while True:
            self.wait_for_next_request()
            if len(kwargs.get('data', {})) > 0 or len(kwargs.get('json', '')) > 0:
                headers = kwargs.get('headers', {})
                if 'Content-Type' not in headers:
                    headers['Content-Type'] = 'application/json'
                kwargs['headers'] = headers
            resp = request_methods[request_type](url, **kwargs)
            if resp.status_code == 202:
                # not yet indexed
                timeout = resp.json()['retry_after']
                self.logger.warning('Channel not yet indexed! Waiting for %f seconds', timeout)
                if not skip_indexing:
                    time.sleep(timeout)
                    continue
            elif resp.status_code == 429:
                # searching too fast, increase timeout by 50ms
                self.timeout += 0.05
                rate_limit = resp.json()['retry_after']
                self.logger.warning('We are being ratelimited! Waiting for %f seconds', rate_limit)
                time.sleep(rate_limit)
                continue
            self.earliest_next_request = time.time() + self.timeout
            return resp


def configure_logging():
    """Initialize the logger"""
    logfile_path = 'deletion.log'
    logging.basicConfig(
        handlers=(logging.FileHandler(logfile_path, delay=True), logging.StreamHandler()),
        level=LOGLEVEL,
        format='%(asctime)s [%(name)-8.8s] [%(levelname)-4.4s]  %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S'
    )
    print(f'Writing logfile to {logfile_path}.')


def to_snowflake(date: datetime.datetime) -> str:
    unix_time_ms = int(date.timestamp() * 1000)
    return str(int((unix_time_ms - 1420070400000) * pow(2, 22)))


def delete_messages(api: Discord,
                    messages: List[Message],
                    skip_channel_ids: List[str],
                    keep_pinned: bool,
                    unarchive_threads: bool) -> None:
    to_delete_messages = [m for m in messages
                          if not (m.channel_id in skip_channel_ids or (keep_pinned and m.pinned))
                         ]
    no_messages = len(to_delete_messages)
    logging.info('Deleting %i messages', no_messages)
    for i, message in enumerate(to_delete_messages):
        eta = datetime.timedelta(seconds=((no_messages - i) * api.timeout))
        retry = True
        while retry:
            retry = False
            try:
                message.delete()
            except MessageDeleteError as exc:
                resp_data = exc.response.json()
                if resp_data.get('code') == 50083 and unarchive_threads:
                    retry = PartialChannel(message.channel_id, message._api).unarchive()
                    if retry:
                        continue
                logging.info(
                    '%i/%i ETA %s | Failed to delete %s with content "%s", Code: %i, Reason: %s, Content: %s',
                    i+1,
                    no_messages,
                    eta,
                    message,
                    message.content,
                    exc.response.status_code,
                    exc.response.reason,
                    exc.response.content
                )
            else:
                logging.info('%i/%i ETA %s | Successfully deleted %s with content "%s"',
                            i+1,
                            no_messages,
                            eta,
                            message,
                            message.content)


def delete_from_json(config: Config):
    user = AuthUser(config.token, config.user_id)
    api = Discord(user)
    before_snowflake = to_snowflake(config.before)
    with open('channels.json', 'r') as fp:
        channels = json.load(fp)

    # this is to start indexing channels so we can retreive messages faster, since it can often be
    # expected that your DM channels have never been indexed
    channel_objects = [PartialChannel(cid, api) for cid in channels.keys()]
    available_channels = []
    for channel in channel_objects:
        availability = channel.check_available()
        logging.info('Channel %s is %s', channel, availability)
        if availability != ChannelAvailability.UNAVAILABLE:
            available_channels.append(channel)

    channels_to_purge = [
        channel for channel in available_channels if channel.id not in config.skip_channels
    ]
    messages = []
    logging.info('Collecting messages for %i channels.', len(channels_to_purge))
    for channel in channels_to_purge:
        messages += channel.get_all_messages(before_snowflake)
    delete_messages(
        api, messages, config.skip_channels, config.keep_pinned, config.unarchive_threads
    )


def delete_in_guilds(config: Config):
    before_snowflake = to_snowflake(config.before)
    user = AuthUser(config.token, config.user_id)
    api = Discord(user)
    guilds = api.get_guilds()
    to_purge_guilds = [g for g in guilds if g.id not in config.skip_guilds]
    messages = []
    logging.info('Collecting messages for %i guilds.', len(to_purge_guilds))
    for guild in to_purge_guilds:
        messages += guild.get_all_messages(before_snowflake)
    delete_messages(
        api, messages, config.skip_channels, config.keep_pinned, config.unarchive_threads
    )


def delete_dms(config: Config):
    before_snowflake = to_snowflake(config.before)
    user = AuthUser(config.token, config.user_id)
    api = Discord(user)
    dm_channels = api.get_dm_channels()
    channels_to_purge = [
        channel for channel in dm_channels if channel.id not in config.skip_channels
    ]
    messages = []
    logging.info('Collecting messages for %i channels.', len(channels_to_purge))
    for channel in channels_to_purge:
        messages += channel.get_all_messages(before_snowflake)
    delete_messages(
        api, messages, config.skip_channels, config.keep_pinned, config.unarchive_threads
    )


def main():
    configure_logging()
    config = Config.from_toml('./config.toml')
    print(config.format())
    if config.from_json:
        delete_from_json(config)
    else:
        if config.purge_guilds:
            delete_in_guilds(config)
        if config.purge_dms:
            delete_dms(config)


if __name__ == '__main__':
    main()
