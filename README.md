# Table of Contents

[[_TOC_]]

# Deprecation Notice

This python tool is unmaintained in favour of my [rust rewrite](https://gitlab.com/pohlarized/discord-auto-delete-rs).

# What is this

This is just a little script that will go through all your guilds, and remove all your messages within that guild.

# Disclaimer
Discord does not allow "self-bots", and usage of automated interaction with the API may lead to account termination.

> Automating normal user accounts (generally called "self-bots") outside of the OAuth2/bot API is forbidden, and can result in an account termination if found.

[source](https://discord.com/developers/docs/topics/oauth2#bots)

I do not guarantee the safety of this tool and any potential implications it may have on you or your account availability.

There have been claims about account termination for other automated deletion tools already, see [here](https://github.com/victornpb/deleteDiscordMessages/issues/193) for example.

# Installation

I recommend using a virtualenv.
You can create one in a directory `./.venv` via
```sh
python -m venv .venv
```
and activate it with
```sh
source .venv/bin/activate
```

To install the package, go into the root directory and execute the command
```sh
pip install .
```

# Usage

You will find an `example_config.toml` file with comments on each value, which you can copy to `config.toml` in order to be accepted by the script.
Two values you *have to* set are the `user_id` and `token`, which you want to fill in with your discord user ID and your discord token (read below on how to receive your token if you don't know).

This is used to read in your credentials to authenticate against the API.

You can also further refine the configuration, here is an example:

```toml
skip_channels = [ 238304102412507838, 538364103411527338, 234104102712707888 ]
skip_guilds = [ 221112996204921192 ]
before = '2020-02-01T12:00'
keep_pinned = false
unarchive_threads = false
from_json = false
purge_guilds = true
purge_dms = false
user_id = 680333322117942450  # has to be filled out!
token = 'nxOwkdkj33kk.NOTAREALTOKEN.93939393hehehe'  # has to be filled out!
```

Afterwards, just run the script with the `discord-auto-delete` command.

## Getting your token

You can paste the following small javascript snippet in your javascript console to receive your token:

```javascript
var win = document.createElement('iframe');
document.body.appendChild(win);
win.contentWindow.localStorage.token;
```

This should display your token in the console. There might be additional quotation marks and escaped quotation marks around your token (like this: `\"`), they are *not* part of your token!

Sometimes this method does not work, in that case you can try [this guide by android authority](https://www.androidauthority.com/get-discord-token-3149920/).

# Potential Issues

This section will address some issues that may arise when using the tool.

## Lots of random messages found

If you find the tool returning a massive amount of messages for a channel/guild and trying to delete messages that are not yours, then you have probably provided an incorrect discord ID.
